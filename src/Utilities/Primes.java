package Utilities;

import java.util.ArrayList;

/**
 * Various prime functions, based upon an Sieve of Eratosthenes to find more
 * primes.
 *
 * TODO: Make it so it's not all or nothing with prime calculation.
 *
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 * @date Dec 17, 2013
 */
public class Primes {

    private static final int MAXPRIME = 2000000;
    private static final boolean sieve[] = new boolean[MAXPRIME];
    //Array list of confirmed primes.
    private static final ArrayList<Integer> primes = new ArrayList<>();

    /**
     * Calculates all primes less than or equal to 2000000. Note that this is a
     * method for pre-calculation, not lookup. Does nothing if n&lt;=0.
     *
     */
    public static void calcPrimes() {
        if (!primes.isEmpty()) {
            return;
        }
        
        for (int i = 3; i < MAXPRIME; i += 2) {
            sieve[i] = true;
        }
        primes.add(2);

        int i;
        for (i = 3; i * i < MAXPRIME; i++) {
            if (sieve[i] == true) {
                primes.add(i);
                for (int j = i + 1; j < MAXPRIME; j++) {
                    if (j == j / i * i) {
                        sieve[j] = false;
                    }
                }
            }
        }

        for (; i < MAXPRIME; i++) {
            if (sieve[i] == true) {
                primes.add(i);
            }
        }
    }

    /**
     * Retrieves the nth prime, calculating it if necessary.
     *
     * @param n the prime desired.
     * @return the nth prime.
     */
    public static long getPrime(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("Prime " + n + " does not exist.");
        }
        calcPrimes();
        return primes.get(n - 1);
    }

    /**
     * Returns a list of the first n primes. Calculates more primes if
     * necessary.
     *
     * @param n the last prime which should be found. Not an index.
     * @return An array list of primes.
     */
    public static ArrayList<Integer> primeList(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("Prime " + n + " does not exist.");
        }
        calcPrimes();
        ArrayList<Integer> results = new ArrayList<>();
        for (int j = 0; j < n; j++) {
            results.add(primes.get(j));
        }
        return results;
    }

    /**
     * Gives a list of primes less than the number given using a bin
     *
     * @param n the upper bound for primes desired.
     * @return A list of primes less than n.
     */
    public static ArrayList<Integer> primesUnder(int n) {
        ArrayList<Integer> results = new ArrayList<>();
        if (n > 1) {
            int cutoff = findNextLowest(n);
            for (int j = 0; j <= cutoff; j++) {
                results.add(primes.get(j));
            }
        }
        return results;
    }

    /**
     * Wrapper method which ensures the primes desired have been calculated
     * before searching.
     *
     * @param n the upper bound on which we are searching for the next lowest
     * prime.
     * @return the index of the greatest prime less than n.
     */
    private static int findNextLowest(int n) {
        if (primes.isEmpty()) {
            calcPrimes();
        }
        return nextLowestSearch(n);
    }

    /**
     * Binary search to find the greatest prime less than n.
     *
     * @param n the prime being searched for.
     * @return the index of the greatest prime less than n.
     */
    private static int nextLowestSearch(int n) {
        int lo = 0;
        int hi = primes.size();
        while (lo <= hi) {
            // Key is in a[lo..hi] or not present.
            int mid = lo + (hi - lo) / 2;
            if (n < primes.get(mid)) {
                hi = mid - 1;
            } else if (n > primes.get(mid)) {
                lo = mid + 1;
            } else {
                return mid;
            }
        }
        return hi;
    }

    /**
     * Computes and lists the prime factors of a number.
     *
     * @param n the number to factorize.
     * @return a list of prime factors in ascending order.
     */
    public static ArrayList<Long> primeFactors(long n) {
        calcPrimes();
        ArrayList<Long> factors = new ArrayList<>();
        long remainder = n;
        int i;
        long curPrime;
        while (remainder != 1) {
            //Iterates through primes, searching for the next number by which n
            //is evenly divisible.
            for (i = 0; i < primes.size(); i++) {
                curPrime = primes.get(i);
                if (remainder % curPrime == 0 || remainder == curPrime) {
                    factors.add(curPrime);
                    remainder = (long) Math.floor(remainder / curPrime);
                    break;
                }
            }
        }
        return factors;
    }
}
