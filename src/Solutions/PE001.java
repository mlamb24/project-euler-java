package Solutions;

/**
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we
 * get 3, 5, 6 and 9. The sum of these multiples is 23.
 *
 * Find the sum of all the multiples of 3 or 5 below 1000.
 *
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 * @date 12-17-2013 (cleaned up)
 */
public class PE001 {

    /**
     * Prints the sum of all numbers less than n divisible by 3 or 5 less than
     * 1000.
     */
    public static void ThreeFiveSum() {
        long start = System.nanoTime();
        int sum = 0;
        for (short i = 1; i < 1000; i++) {
            if (i % 3 == 0 || i % 5 == 0) {
                sum += i;
            }
        }
        long end = System.nanoTime();
        System.out.println(sum);
        System.out.printf("Runtime: %.6f ms \n", (end - start) / Math.pow(10, 6));

    }
}