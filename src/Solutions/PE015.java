package Solutions;

/**
 * Starting in the top left corner of a 2×2 grid, and only being able to move to
 * the right and down, there are exactly 6 routes to the bottom right corner.
 *
 * How many such routes are there through a 20×20 grid?
 *
 * @author Michael Lambert <mlamb24@tigers.lsu.edu>
 * @date 12-24-2013 (Redone)
 */
public class PE015 {

    /**
     * Gives the product of a series from each endpoint inclusive.
     *
     * @param start The starting point of the series to be multiplied. Should be
     * less than end value.
     * @param end The end point of the series to be multiplied. Should be less
     * than start value.
     * @return the product of each number in [start, end]; -1 for an invalid
     * parameter.
     */
    public static double seriesMult(int start, int end) {
        if (start < 0 || start >= end) return -1;
        double ans = 1;
        for (int n = start; n <= end; n++) {
            ans = ans * n;
        }
        return ans;
    }

    /**
     * Returns the number groupings of c objects in n choices, with order not
     * considered.
     *
     * @param n the number of objects to choose from.
     * @param c the number of objects in each grouping.
     * @return 0 if n&lt;c, n&lt;=0, c&lt;0, -1 if c==0, the number of
     * order-independent groupings otherwise.
     */
    public static double combination(int n, int c) {
        if (n <= 0 || c < 0 || n < c) return 0;
        if (n == c) return 1;
        if (c == 0) return -1;
        return seriesMult(n - c + 1, n) / seriesMult(1, c);
    }

    //Finding number of paths to bottom right: (a+b)! / a!b!
    public static void solution() {
        int x = 20, y = 20;
        long start = System.nanoTime();
        System.out.println("Number of paths in a " + x + "x" + y + " grid is "
                + (long) combination(x + y, x) + ".");
        long end = System.nanoTime();
        System.out.printf("Runtime: %.6f ms\n", (end - start) / Math.pow(10, 6));
    }
}
