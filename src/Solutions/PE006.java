package Solutions;

/**
 * The sum of the squares of the first ten natural numbers is, 12 + 22 + ... +
 * 102 = 385
 *
 * The square of the sum of the first ten natural numbers is, (1 + 2 + ... +
 * 10)2 = 552 = 3025
 *
 * Hence the difference between the sum of the squares of the first ten natural
 * numbers and the square of the sum is 3025 − 385 = 2640.
 *
 * Find the difference between the sum of the squares of the first one hundred
 * natural numbers and the square of the sum.
 *
 * @author Michael Lambert <mlamb24@tigers.lsu.edu>
 * @date 12-20-2013
 */
public class PE006 {

    /**
     * Calculates the sum of numbers 1...i + the sum of the squares of 1...i.
     * @param i the upper limit of the summation, inclusive.
     * @return the sum of numbers 1...i + the sum of the squares of 1...i.
     */
    public static int sumSquaredPlusSumOfSquares(int i) {
        //Sum of numbers 1...i
        int sum = i * (i + 1) / 2;
        //Sum of 1^2...i^2
        int sqsum = (2 * i + 1) * (i + 1) * i / 6;
        return sum * sum - sqsum;
    }

    /**
     * Prints sumSquaredPlusSumOfSquares(100) and runtime.
     */
    public static void solution() {
        long start = System.nanoTime();
        System.out.println(sumSquaredPlusSumOfSquares(100));
        long end = System.nanoTime();
        System.out.printf("Runtime: %.6f ms \n", (end - start) / Math.pow(10, 6));
    }
}
