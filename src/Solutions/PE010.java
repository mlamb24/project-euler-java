package Solutions;

import Utilities.Primes;

/**
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 *
 * Find the sum of all the primes below two million.
 *
 * @author Michael Lambert <mlamb24@tigers.lsu.edu>
 * @date 12-22-2013 (cleaned up)
 */
public class PE010 {

    public static long sumOfPrimes(int n) {
        long sum = 0;
        for (int i = 1; Primes.getPrime(i) < n; i++) {
            sum += Primes.getPrime(i);
        }
        return sum;
    }

    public static void solution() {
        long start = System.nanoTime();
        System.out.println(sumOfPrimes(2000000));
        long end = System.nanoTime();
        System.out.printf("Runtime: %.6f ms\n", (end - start) / Math.pow(10, 6));
    }
}
