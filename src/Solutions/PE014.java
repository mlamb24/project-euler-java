package Solutions;

/**
 * The following iterative sequence is defined for the set of positive integers:
 *
 * n → n/2 (n is even) 
 * n → 3n + 1 (n is odd)
 *
 * Using the rule above and starting with 13, we generate the following
 * sequence: 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
 *
 * It can be seen that this sequence (starting at 13 and finishing at 1)
 * contains 10 terms. Although it has not been proved yet (Collatz Problem), it
 * is thought that all starting numbers finish at 1.
 *
 * Which starting number, under one million, produces the longest chain?
 *
 * NOTE: Once the chain starts the terms are allowed to go above one million.
 *
 * @author Michael Lambert <mlamb24@tigers.lsu.edu>
 * @date 12-23-2013 (Optimized)
 */
public class PE014 {

    private static final int SIZE = 10000000;
    private static final short[] sequences = new short[SIZE];

    public static short collatz(long n) {
        if (n == 1) {
            return 0;
        }
        if (n >= SIZE) {
            if (n % 2 == 0) {
                return (short) (1 + collatz(n / 2));
            } else {
                return (short) (1 + collatz(3 * n + 1));
            }
        }
        if (sequences[(int) n] == 0) {
            if (n % 2 == 0) {
                sequences[(int) n] = (short) (1 + collatz(n / 2));
            } else {
                sequences[(int) n] = (short) (1 + collatz(3 * n + 1));
            }
        }
        return sequences[(int) n];
    }

    public static void solution() {
        long start = System.nanoTime();
        int maxSteps = 0, curSteps;
        long curNum = 0;
        for (int l = 2; l < 1000000; l++) {
            curSteps = collatz(l);
            if (curSteps > maxSteps) {
                curNum = l;
                maxSteps = curSteps;
            }
        }
        long end = System.nanoTime();
        System.out.println("The number with the longest Collatz sequence is " + curNum + " with " + maxSteps + " steps.");
        System.out.printf("Runtime: %.6f ms\n", (end - start) / Math.pow(10, 6));
    }
}
