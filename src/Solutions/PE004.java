package Solutions;

/**
 * A palindromic number reads the same both ways. The largest palindrome made
 * from the product of two 2-digit numbers is 9009 = 91 × 99.
 *
 * Find the largest palindrome made from the product of two 3-digit numbers.
 *
 * @author Michael Lambert <mlamb24@tigers.lsu.edu>
 * @date 12-20-2013 (Cleaned up)
 */
public class PE004 {

    public static boolean isPal(int n) {
        char[] digits = (Integer.toString(n)).toCharArray();
        for (int i = 0; i < digits.length / 2; i++) {
            if (digits[i] != digits[digits.length - i - 1]) {
                return false;
            }
        }
        return true;
    }

    public static void solution() {
        long start = System.nanoTime();
        int maxPal = 0, curProd = 0, facOne = 0, facTwo = 0;
        for (int i = 999; i >= 100; i--) {
            for (int j = i; j >= 100; j--) {
                curProd = i * j;
                if (curProd <= maxPal) {
                    break;
                }
                if (isPal(curProd)) {
                    facOne = i;
                    facTwo = j;
                    maxPal = Math.max(curProd, maxPal);
                }
            }
        }
        long end = System.nanoTime();
        System.out.println("Largest palindromic product of 2 3-digit numbers: " + maxPal + " (" + facOne + "x" + facTwo + ")");
        System.out.printf("Runtime: %.6f ms\n", (end - start) / Math.pow(10, 6));
    }
}
