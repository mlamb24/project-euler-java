package Solutions;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.Scanner;

/**
 * Work out the first ten digits of the sum of the following one-hundred
 * 50-digit numbers.
 *
 * @author Michael Lambert <mlamb24@tigers.lsu.edu>
 * @date 12-23-2013 (redone)
 */
public class PE013 {

    private static final String fileName = "PE013.txt";

    public static void solution() {
        try {
            long start = System.nanoTime();
            Scanner input = new Scanner(new File(fileName));
            BigInteger sum = new BigInteger(input.nextLine());
            while (input.hasNextLine()) {
                sum = sum.add(new BigInteger(input.nextLine()));
            }
            long end = System.nanoTime();
            System.out.println("The sum of the numbers is " + sum.toString().substring(0, 10));
            System.out.printf("Runtime: %.6f ms \n", (end - start) / Math.pow(10, 6));
        } catch (FileNotFoundException ex) {
            System.out.println("File " + fileName + "not found.");
        }
    }
}
