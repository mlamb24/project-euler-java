package Solutions;

import Utilities.Primes;
import java.util.ArrayList;

/**
 * The prime factors of 13195 are 5, 7, 13 and 29.
 *
 * What is the largest prime factor of the number 600851475143 ?
 *
 * @author Michael Lambert <mlamb24@tigers.lsu.edu>
 * @date 12-17-2013 (Cleaned up)
 */
public class PE003 {

    public static void largestFactor() {
        long test = 600851475143L;
        long start = System.nanoTime();
        ArrayList<Long> factors = Primes.primeFactors(test);
        long end = System.nanoTime();

        System.out.println(factors.get(factors.size() - 1));
        System.out.printf("Runtime: %.6f ms \n", (end - start) / Math.pow(10, 6));
        //Runtime includes finding primes for prime factor method.
    }
}
