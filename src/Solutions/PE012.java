package Solutions;

/**
 * The sequence of triangle numbers is generated by adding the natural numbers.
 * So the 7th triangle number would be 1 + 2 + 3 + 4 + 5 + 6 + 7 = 28. The first
 * ten terms would be:
 *
 * 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
 *
 * Let us list the factors of the first seven triangle numbers: ... We can see
 * that 28 is the first triangle number to have over five divisors.
 *
 * What is the value of the first triangle number to have over five hundred
 * divisors?
 *
 * @author Michael Lambert <mlamb24@tigers.lsu.edu>
 * @date 12-22-2013 (redone)
 */
public class PE012 {

    public static int[] divCounts = new int[13000];

    /**
     * Brute force method. Sufficient in this case as our checked numbers will
     * always be relatively small.
     *
     * @param n the number of which to find the number of divisors.
     * @return the number of divisors of n
     */
    public static int divCount(int n) {
        if (divCounts[n] != 0) {
            return divCounts[n];
        }

        int curDiv = 1;
        int numDiv = 0;
        int sqrt = (int) Math.round(Math.sqrt(n));
        for (; curDiv <= sqrt; curDiv++) {
            if (n % curDiv == 0) {
                numDiv += 2;
            }
        }
        if (sqrt * sqrt == n) {
            numDiv--;
        }
        divCounts[n] = numDiv;
        return numDiv;
    }

    /**
     * Finds the first triangle number with over n divisors.
     *
     * @param numDiv the number of divisors we would like to exceed
     * @return the triangle number with >n divisors.
     */
    public static int triDiv(int numDiv) {
        int divisors, curTri = 1;
        while (true) {
            if (curTri % 2 == 0) {
                divisors = divCount(curTri / 2) * divCount(curTri + 1);
            } else {
                divisors = divCount(curTri) * divCount((curTri + 1) / 2);
            }
            if (divisors > numDiv) {
                return (curTri * (curTri + 1) / 2);
            }
            curTri++;
        }
    }

    public static void solution() {
        long start = System.nanoTime();
        System.out.println(triDiv(500));
        long end = System.nanoTime();
        System.out.printf("Runtime: %.6f ms \n", (end - start) / Math.pow(10, 6));
    }
}
