package Solutions;

/**
 * 2520 is the smallest number that can be divided by each of the numbers from 1
 * to 10 without any remainder.
 *
 * What is the smallest positive number that is evenly divisible by all of the
 * numbers from 1 to 20?
 *
 * @author Michael Lambert <mlamb24@tigers.lsu.edu>
 * @date 12-19-2013 (Cleaned up)
 */
public class PE005 {

    public static int firstDivisibleByAll(int n) {
        //Checknum could be chosen better, but not really important since this
        //is a brute force method unconcerned with being the most efficient
        //solution.
        int checkNum = n + 1, divisor;
        boolean done = false;
        while (!done) {
            done = true;
            divisor = n / 2 + 1;
            do {
                if (checkNum % divisor++ != 0) {
                    done = false;
                    checkNum++;
                }
            } while (divisor <= n && done);
        }
        return checkNum;
    }

    public static void firstDivisible() {
        long start = System.nanoTime();
        System.out.println(firstDivisibleByAll(20));
        long end = System.nanoTime();

        System.out.printf("Runtime: %.6f ms \n", (end - start) / Math.pow(10, 6));
    }
}
