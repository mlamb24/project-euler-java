package Solutions;


/**
 * A Pythagorean triplet is a set of three natural numbers, a &lt; b &lt; c, for
 * which, a^2 + b^2 = c^2
 *
 * For example, 3^2 + 4^2 = 9 + 16 = 25 = 52.
 *
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find
 * the product abc.
 *
 * @author Michael Lambert <mlamb24@tigers.lsu.edu>
 * @date 12-20-2013 (Redone)
 */
public class PE009 {

    public static void tripletProduct(int s) {
        int a, b, c;
        for (a = 3; a <= (s - 3) / 3; a++) {
            for (b = 1; b <= (s - 1 - a) / 2; b++) {
                c = s - a - b;
                if (c * c == a * a + b * b) {
                    System.out.println(a + "*" + b + "*" + c + " = " + (long) a * b * c);
                }
            }
        }
    }

    public static void solution() {
        long start = System.nanoTime();
        tripletProduct(1000);

        long end = System.nanoTime();
        System.out.printf("Runtime: %.6f ms \n", (end - start) / Math.pow(10, 6));
    }
}
