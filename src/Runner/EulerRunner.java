package Runner;

import Solutions.*;
import Utilities.Primes;
import java.util.Scanner;

public class EulerRunner {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.print("Precalculating primes...");
        long startPrimes = System.nanoTime();
        Primes.calcPrimes();
        long endPrimes = System.nanoTime();
        System.out.printf("Calculated all primes under 2*10^6 in %.6f ms \n",
                (endPrimes - startPrimes) / Math.pow(10, 6));
        while (true) {
            Scanner input = new Scanner(System.in);
            System.out.print("Problem #: ");
            if (input.hasNextInt()) {
                int n = input.nextInt();
                switch (n) {
                    case 1:
                        PE001.ThreeFiveSum();
                        break;
                    case 2:
                        PE002.EvenFibSum();
                        break;
                    case 3:
                        PE003.largestFactor();
                        break;
                    case 4:
                        PE004.solution();
                        break;
                    case 5:
                        PE005.firstDivisible();
                        break;
                    case 6:
                        PE006.solution();
                        break;
                    case 7:
                        //Problem 7 simply asks for the 10001st prime.
                        long start = System.nanoTime();
                        System.out.println(Primes.getPrime(10001));
                        long end = System.nanoTime();
                        System.out.printf("Runtime: %.6f ms \n", (end - start) / Math.pow(10, 6));
                        break;
                    case 8:
                        PE008.solution();
                        break;
                    case 9:
                        PE009.solution();
                        break;
                    case 10:
                        PE010.solution();
                        break;
                    case 11:
                        PE011.solution();
                        break;
                    case 12:
                        PE012.solution();
                        break;
                    case 13:
                        PE013.solution();
                        break;
                    case 14:
                        PE014.solution();
                        break;
                    case 15:
                        PE015.solution();
                        break;
                    default:
                        System.out.println("Unsolved");

                }
            } else if (input.hasNext() && input.next().equalsIgnoreCase("exit")) {
                break;
            } else {
                System.out.println("Invalid input.");
            }
        }
    }
}
